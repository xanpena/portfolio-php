<?php 

ini_set('display_errors', 1);
ini_set('display_starup_error', 1);
error_reporting(E_ALL);

require_once '../vendor/autoload.php'; 

$dotenv = Dotenv\Dotenv::create(__DIR__ . '/..');
$dotenv->load();

use Illuminate\Database\Capsule\Manager as Capsule;
use Aura\Router\RouterContainer;

$capsule = new Capsule;

$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => getenv('DB_HOST'),
    'database'  => getenv('DB_DATABASE'),
    'username'  => getenv('DB_USER'),
    'password'  => getenv('DB_PASSWORD'),
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);

// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal();

// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent();

$request = Zend\Diactoros\ServerRequestFactory::fromGlobals(
    $_SERVER,
    $_GET,
    $_POST,
    $_COOKIE,
    $_FILES
);

// var_dump($request->getUri()->getPath());

$routerContainer = new RouterContainer();
$map = $routerContainer->getMap();

$map->get('index', '/portfolio-php/', [
    'controller' => 'App\Controllers\IndexController',
    'action' => 'indexAction'
]);

$map->get('addJob', '/portfolio-php/jobs/add', [
    'controller' => 'App\Controllers\JobController',
    'action' => 'getAddJobAction'
]);

$map->post('saveJob', '/portfolio-php/jobs/add', [
    'controller' => 'App\Controllers\JobController',
    'action' => 'getAddJobAction'
]);

$matcher = $routerContainer->getMatcher();
$route = $matcher->match($request);
if ( !$route ) {
    // Error 404
    echo 'no route';
} else {
    //require $route->handler;
    $handlerData = $route->handler;
    $actionName =$handlerData['action'];
    $controller = new $handlerData['controller'];
    $response = $controller->$actionName($request);

    echo $response->getBody();
}

/*var_dump($route); 
echo '<hr>';
var_dump($route->handler); */