<?php

namespace App\Controllers;

use \App\Models\Job;
use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;

class JobController extends BaseController {

    public function getAddJobAction($request) {
        
        if($request->getMethod() == 'POST'){
            /*var_dump($_POST);
            var_dump($request->getMethod());
            var_dump($request->getBody());
            var_dump($request->getParsedBody());*/

            $postData = $request->getParsedBody();

            // https://respect-validation.readthedocs.io

            $jobValidator = v::key('title', v::stringType()->length(1, 32))
                ->key('description', v::stringType()->length(1, 150));
            
            try{
                $jobValidator->assert($postData);

                $files = $request->getUploadedFiles();
                //var_dump($files); exit;
                $logo = $files['logo'];

                if($logo->getError() == UPLOAD_ERR_OK){
                    $fileName = $logo->getClientFilename();
                    $logo->moveTo("uploads/$filename");
                }

                $job = new Job();
                $job->title = $postData['title'];
                $job->description = $postData['description'];
                $job->save();

            }catch(NestedValidationException  $exception) {
                $responseMessage = $exception->getMainMessage();
                var_dump($exception->getFullMessage());
            }
            
        }

        return $this->renderHTML('addJob.twig');
    }
    
}