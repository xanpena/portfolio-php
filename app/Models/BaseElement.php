<?php 

namespace App\Models;

class BaseElement {

    private $title;
    private $description;
    private $visible;
    private $months;

    public function __construct($title, $description, $visible, $months) {
        $this->title = $title;
        $this->description = $description;
        $this->visible = $visible;
        $this->months = $months;
    }

    // Getters properties

    public function getTitle() {
        return $this->title;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getVisible() {
        return $this->visible;
    }

    public function getMonths() {
        return $this->months;
    }

    // Setters properties

    public function setTitle($value){
        if($value == ''){
            $value = 'N/A';
        }
        $this->title = $value;
        return true;
    }

    public function setDescription($value){
        $this->description = $value;
        return true;
    }

    public function setVisible($value){
        $this->visible = $value;
        return true;
    }

    public function setMonths($value){
        $this->months = $value;
        return true;
    }

    // Methods

    public function getDurationAsString(){
        $years = floor($this->months / 12);
        $extraMonths = $this->months % 12;

        return "$years years $extraMonths months";
    }
}